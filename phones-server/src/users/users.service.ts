import { Injectable } from '@nestjs/common';

export type User = any;

@Injectable()
export class UsersService {
  private readonly users = [
    {
      userId: 1,
      username: 'user1',
      password: 'password',
      token: '',
    },
    {
      userId: 2,
      username: 'user2',
      password: 'password',
      token: '',
    },
    {
      userId: 3,
      username: 'user3',
      password: 'password',
      token: '',
    },
  ];

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find(user => user.username === username);
  }

  saveToken(username: string, token: string) {
    var user = this.users.find(user => user.username === username);
    user.token = token;
    const index = this.users.findIndex(x => x.userId === user.userId);
    if (index > -1) {
      this.users.splice(index, 1);
      this.users.push(user);
    }
  }

  getTokens(): any[] {
    const tokens = [];
    for (let user of this.users) {
      if (user.token != '') {
        tokens.push(user.token);
      }
    }
    return tokens;
  }
}
