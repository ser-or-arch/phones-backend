import { Controller, Get, Param, UseGuards } from '@nestjs/common';

import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('currency')
export class CurrencyController {
  private client: ClientProxy;

  constructor() {
    this.client = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: 'currency',
        port: 8081,
      },
    });
  }

  @UseGuards(JwtAuthGuard)
  @Get(':valueInDollar')
  findAll(@Param('valueInDollar') valueInDollar: string): Promise<string> {
    return this.client
      .send<string, string>('getDollarInRon', valueInDollar)
      .toPromise();
  }
}
