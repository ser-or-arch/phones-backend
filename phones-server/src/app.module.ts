import { Module } from '@nestjs/common';
import { PhoneModule } from './phones/phone.module';
import { CurrencyModule } from './currency/currency.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    PhoneModule,
    CurrencyModule,
    AuthModule,
    UsersModule,
  ],
})
export class AppModule {}
