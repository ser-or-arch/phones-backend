import admin from 'firebase-admin';

import { googleApplicationCredentials } from './utils/constants';

const serviceAccount = require(googleApplicationCredentials);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'your-database-url-here',
});

export const messaging = admin.messaging();
