export interface Phone {
  _id: string;
  name: string;
  description: string;
  price: number;
}
