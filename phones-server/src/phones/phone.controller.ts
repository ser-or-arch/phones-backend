import {
  Body,
  Controller,
  Delete,
  Get,
  GoneException,
  NotFoundException,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';

import { Phone } from './phone.interface';
import { PhoneDto } from './phone.dto';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { sendNotificationToClient } from '../notifications/notify';
import { UsersService } from '../users/users.service';

@Controller('phone')
export class PhoneController {
  private client: ClientProxy;

  constructor(private usersService: UsersService) {
    this.client = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: 'phones',
        port: 8080,
      },
    });
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(): Promise<Phone[]> {
    return this.client.send<Phone[]>('getAll', '').toPromise();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string): Promise<Phone> {
    const phone = this.client.send<Phone, string>('getOne', id).toPromise();
    if (!phone) {
      throw new NotFoundException();
    }
    return phone;
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() phoneDto: PhoneDto): Promise<Phone> {
    const tokens = this.usersService.getTokens();
    const notificationData = {
      title: 'New phone added!',
      body: 'Name: ' + phoneDto.name + ' Description: ' + phoneDto.description,
    };
    console.log(tokens);
    sendNotificationToClient(tokens, notificationData);
    return this.client.send<Phone, PhoneDto>('add', phoneDto).toPromise();
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() phone: Phone): Promise<Phone> {
    const actPhone = this.client.send<Phone, string>('getOne', id).toPromise();
    if (!actPhone) {
      throw new GoneException();
    }
    const data = {
      id: id,
      phone: phone,
    };
    return this.client.send<Phone, any>('update', data).toPromise();
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  delete(@Param('id') id: string): Promise<Boolean> {
    const phone = this.client.send<Phone, string>('getOne', id).toPromise();
    if (!phone) {
      throw new GoneException();
    }
    return this.client.send<Boolean, string>('delete', id).toPromise();
  }
}
