import { Module } from '@nestjs/common';
import { UsersModule } from 'src/users/users.module';
import { PhoneController } from './phone.controller';

@Module({
  imports: [UsersModule],
  controllers: [PhoneController],
})
export class PhoneModule {}
