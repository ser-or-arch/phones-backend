# Phones Software System

## Table of Contents

- [Phones Software System](#phones-software-system)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Architecture](#architecture)
    - [Environment](#environment)
  - [Application Server](#application-server)
    - [Center Server](#center-server)
    - [Phone Microservice](#phone-microservice)
    - [Currency Microservice](#currency-microservice)
  - [Web Server](#web-server)
  - [Communication](#communication)

## Overview

The main purpose of the system is to provide to the users a web page, where they can find a list of different phones, with they description and price. They can modify the list, adding or deleting phones and updating them.

**The parts of the system:**

- Application Server - Procide and handle the data.
- MongoDB DataBase - Store the data.
- Web Server - The platform for users, for use the fetures of the project.
- Firebase Cloud Messaging API - for notifications

## Architecture

```mermaid
graph TD;
A[Web Server] -->|Http Requests| B[Center Application Server]
B -->|TCP Socket Connection| C[Currency Microservice]
B -->|TCP Socket Connection| D[Phones Microservice]
D -->|Requesting Data| E[MongoDB]
C -->|Http Request| F[Live Currency Thirdparty API]
A -->|Trigger Push Notification| B
B -->|Trigger Push Notification| G[FireBase Cloud Messaging API]
G -->|Send Push Notification| A
```

- For creating the Application Server, Nest JS is used, which is a framework for building efficient scalable Node.js web applications.
- The Web Server was written in React JS.
- For sending and receiving notifications, Firebase is used.
- For storing data, Mongo DB is used.
- The required data for the currency conversion is come from a 3rd Party API Server.

### Environment

![Environment](SOA_ENVIRONMENT.png)

The system is running in a Docker Container.
In the container there are more running services:

- "phoneserver": Center server.
- "phones": Microservice for CRUD methods of phones.
- "currency": Microservice for currency conversion.
- "frontend": The React JS applciation.
- "mongo": The MongoDB server.

## Application Server

The Application Server is written is Nest JS and it has three main parts.
The Microservice SOA Pattern is used, so there are different microservices for different datas. One microservice for the phones, one for the currency conversion values. The third part is a center server, which is the bridge between clients and microservices.

### Center Server

This is an API server, which has more endpoint for the web application.
This server handle the HTTP requests coming from Web Application and forward them, with using TCP connection, to the right microservice.
The server also manages the sending of push notifications, with using Firebase. It stores the firebase tokens of the users, and send them a notification every time, when a new phone is added to the list.
Already contains the authentication and authorization with using Json Web Tokens (JWT).
There are two types of strategy:

- "**local.strateg**y", which manages the logging in to the Web Application.
- "**jwt.strategy**", which checks that the user, who executes a request is already logged in or not.

The endpoints of the Center Server:

- "**auth/login**": check the login credentials of the user. **POST** request, the body contains the "username, password" information.
- "**setToken**": Set the Firebase Token fo the client. **POST** request, contains in body the "username" and the "token"
- "**currency**": make the conversion from _dollar_ to _lei_ with the current conversion value. **GET** request, contains the values of the dollars as a parameter ("valueInDollar").
- "**phone**": CRUD methods for pones.
  - **GET**: Get all phones from the Database.
  - **GET**: Contains the ID as a parameter ("id"). Returen the phone with that ID.
  - **POST**: Creating new phone. Contains a phoneDto in body.
  - **PUT**: Updating a phone. Contains a phone in body and their ID as a paramater("id").
  - **DELETE**: Delete a phone. Contains the ID as a parameter ("id").

```mermaid
classDiagram
    AppModule <-- PhoneModule
    AppModule <-- UsersModule
    AppModule <-- AuthModule
    AppModule <-- CurrencyModule
    CurrencyModule <-- CurrencyController
    PhoneModule <-- PhoneController
    PhoneModule <-- UsersModule
    AuthModule <-- UsersModule
    AuthModule <-- PassportModule
    AuthModule <-- JwtModule
    AuthModule <-- AuthService
    AuthModule <-- AuthController
    UsersModule <-- UserService
```

### Phone Microservice

Is a Nest JS microservice, using TCP conenction, for prividing data about phones from a MongoDB database. It can be uset by TCP conenction, with using the special keywords: **"getAll", "getOne", "add", "update", "delete"**.

```mermaid
classDiagram
    AppModule <-- MongooseModule
    AppModule <-- PhoneModule
    PhoneModule <-- PhoneController
    PhoneModule <-- PhoneService
```

### Currency Microservice

Is a Nest JS microservice, using TCP connection. It send a request to a 3rd Party API server: "Currency API" for get the conversion value from **dollar** to **lei**. With the free Currency API Key, the server cans end 1250 request free per month. If this is not enough, it can be updated to a premium account. The keyword for the TCP connection is **"getDollarInRon"**.

```mermaid
classDiagram
    CurrencyModule <-- HttpModule
    CurrencyModule <-- CurrencyController
    CurrencyModule <-- CurrencyService

```

## Web Server

The Web Server is created with using React JS. It provide a visual interface for users, to use the Application Server.
The users firstly have to log in to the Web Page, and after this, they will got a JWT access token, which one wil help them to reach the other secure endpoints.
After logging in, the user will see the main page of Web Server, which is a table, contains the list of phones from database. The user can create new phones and delete or update the existing ones.
With Log In, the user also subscribe for notifications. If a user add a new phone to the list, all users will got a push notification about it.

The communication between the web page and application server is happening by Http Requests.

```mermaid
classDiagram
    App <-- LoginForm
    App <-- PhonePage
    LoginForm <-- LoginService
    PhonePage <-- PhoneService
    PhonePage <-- CurrencyService

```

## Communication

There are different types of communication between components:

- Web Server <--> Center Server - **Http Requests**
- Web Servevr --> Firebase Cloud Messaging API - **Http Requests**
- Center Server <--> Phones Microservice - **TCP Socket Connection**
- Center Servver <--> Currency Microservice - **TCP Socket Connection**
- Currency Microservice <--> Currency API - **Http Requests**
