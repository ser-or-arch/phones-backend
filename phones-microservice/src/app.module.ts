import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { db_host, db_name } from './config';
import { PhoneModule } from './phones/phone.module';

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb://${db_host}/${db_name}`),
    PhoneModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
