import * as mongoose from 'mongoose';

export const PhoneSchema = new mongoose.Schema({
  name: { type: String },
  description: { type: String },
  price: { type: String },
});
