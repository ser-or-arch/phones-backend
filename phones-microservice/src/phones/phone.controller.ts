import { Controller, GoneException, NotFoundException } from '@nestjs/common';
import { IPhone } from './phone.interface';
import { PhoneDto } from './phone.dto';
import { PhoneService } from './phone.service';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class PhoneController {
  constructor(private readonly phoneService: PhoneService) {
    this.phoneService.create({
      name: 'New Phone 1',
      description: 'Description of new phone 1',
      price: 11000,
    });
  }

  @MessagePattern('getAll')
  async findAll(): Promise<IPhone[]> {
    return this.phoneService.findAll();
  }

  @MessagePattern('getOne')
  async findOne(id: string): Promise<IPhone> {
    const phone = this.phoneService.findOne(id);
    if (!phone) {
      throw new NotFoundException();
    }
    return phone;
  }

  @MessagePattern('add')
  async create(phoneDto: PhoneDto): Promise<IPhone> {
    return this.phoneService.create(phoneDto);
  }

  @MessagePattern('update')
  async update(data: any): Promise<IPhone> {
    const id = data.id;
    const phone = data.phone;
    const actPhone = this.phoneService.findOne(id);
    if (!actPhone) {
      throw new GoneException();
    }
    return this.phoneService.update(phone, id);
  }

  @MessagePattern('delete')
  async delete(id: string): Promise<Boolean> {
    const phone = this.phoneService.findOne(id);
    if (!phone) {
      throw new GoneException();
    }
    return this.phoneService.delete(id);
  }
}
