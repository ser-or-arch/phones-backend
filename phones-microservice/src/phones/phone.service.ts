import { Injectable } from '@nestjs/common';
import { PhoneDto } from './phone.dto';
import { IPhone } from './phone.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class PhoneService {
  constructor(@InjectModel('Phone') private readonly model: Model<IPhone>) {
    this.create({ name: 'Samsung', description: 'Goood Phone', price: 1000 });
  }

  async findAll(): Promise<IPhone[]> {
    return this.model.find().exec();
  }

  async findOne(id: string): Promise<IPhone> {
    return await this.model.findById(id);
  }

  async create(phoneDto: PhoneDto): Promise<IPhone> {
    const phone = new this.model(phoneDto);
    return await phone.save();
  }

  async update(phone: IPhone, id: string): Promise<IPhone> {
    this.delete(id);
    return this.create(phone);
  }

  async delete(id: string): Promise<Boolean> {
    const actPhone = await this.model.findById(id);
    if (!actPhone) return false;
    await this.model.findByIdAndRemove(id);
    return true;
  }
}
