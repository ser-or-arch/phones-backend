import { IsString, IsNumber } from 'class-validator';

export class PhoneDto {
  @IsString() name: string;
  @IsString() description: string;
  @IsNumber() price: number;
}
