import { Document } from 'mongoose';

export interface IPhone extends Document {
  _id: string;
  name: string;
  description: string;
  price: number;
}
