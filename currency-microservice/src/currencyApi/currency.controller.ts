import { Controller } from '@nestjs/common';
import { CurrencyService } from './currency.service';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class CurrencyController {
  constructor(private readonly currencyService: CurrencyService) {}

  @MessagePattern('getDollarInRon')
  async getConversionValueToLei(priceInEuro: string): Promise<String> {
    var priceNumber: number = +priceInEuro;
    return (await this.currencyService.getPriceInRon(priceNumber)).toString();
  }
}
