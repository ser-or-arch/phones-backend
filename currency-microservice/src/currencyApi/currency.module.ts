import { CurrencyController } from './currency.controller';
import { CurrencyService } from './currency.service';
import { CacheModule, HttpModule, Module } from '@nestjs/common';

@Module({
  imports: [HttpModule],
  controllers: [CurrencyController],
  providers: [CurrencyService],
})
export class CurrencyModule {}
