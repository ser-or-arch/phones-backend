import { CONSTANTS } from '../utils/constants';
import { HttpService, Injectable } from '@nestjs/common';

@Injectable()
export class CurrencyService {
  constructor(private httpService: HttpService) {}

  async getPriceInRon(priceInDollar: number): Promise<number> {
    var url = CONSTANTS.CURRENCY_RATES_BASE_URL + CONSTANTS.CURRENCY_API_KEY;
    const response = await this.httpService.get(url).toPromise();

    const data = response.data;
    var rate = data.rates.RON;
    var rateNumber: number = +rate;

    if (rateNumber === NaN) {
      return priceInDollar * CONSTANTS.DEFAULT_DOLLAR_TO_LEI_RATE;
    }

    return priceInDollar * rateNumber;
  }
}
