import { NestFactory } from '@nestjs/core';
import { CurrencyModule } from './currencyApi/currency.module';
import { Logger } from '@nestjs/common';
import { Transport } from '@nestjs/microservices';

const logger = new Logger('Main');

const microserviceOptions = {
  transport: Transport.TCP,
  options: {
    host: 'currency',
    port: '8081',
  },
};

async function bootstrap() {
  const app = await NestFactory.createMicroservice(
    CurrencyModule,
    microserviceOptions,
  );
  app.listen(() => {
    logger.log('Microservice is listening...');
  });
}
bootstrap();
